package com.atb.palindrome;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import com.atb.palindrome.controller.PalindromeController;
import com.atb.palindrome.service.CalculatePalindromeService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class PalindromeApplicationTests {

    
    @Autowired
    private PalindromeController palindromeController;

    @Autowired
    private CalculatePalindromeService calculatePalindromeServicevice;

    @Test
    public void contextLoads() throws Exception {
        assertThat(palindromeController).isNotNull();
        assertThat(calculatePalindromeServicevice).isNotNull();
    }
    
    @Test
    public void calculatePalindromeTest() {
    	int testInput = 12345; //it should return 12321
    	int expected = 12321;
    	int largestInteger = 2147483647;
    	int expectedPalindrome = 2147447412;
    	Assert.assertEquals(expected, calculatePalindromeServicevice.closestPalindrome(testInput)); 
    	Assert.assertEquals(expectedPalindrome, calculatePalindromeServicevice.closestPalindrome(largestInteger));
    }
    
    @Test
    public void calculatePalindromeBadRequestTest() {
    	String testInput = "12345end"; //it should return response of HTTP status of badrequest, 400
    	String testInputTooLarge = "123451376453"; //too large input
    	Assert.assertEquals(HttpStatus.BAD_REQUEST, palindromeController.efficientPalindrome(testInput).getStatusCode()); 
    	Assert.assertEquals(HttpStatus.BAD_REQUEST, palindromeController.efficientPalindrome(testInputTooLarge).getStatusCode());
    }
    
    @Test
    public void palindromeSimpleMethodTest() {
    	int testInput = 12345; //it should return 12321
    	int expected = 12321;
    	int largestInteger = 2147483647;
    	int expectedPalindrome = 2147447412;
    	Assert.assertEquals(expected, calculatePalindromeServicevice.closestPalindrome(testInput)); 
    	Assert.assertEquals(expectedPalindrome, calculatePalindromeServicevice.closestPalindrome(largestInteger));
    }
    
    @Test
    public void palindromeSimpleMethodBadRequestTest() {
    	String testInput = "12345end"; //it should return response of HTTP status of badrequest, 400
    	String testInputTooLarge = "123451376453"; //too large input
    	Assert.assertEquals(HttpStatus.BAD_REQUEST, palindromeController.palindrome(testInput).getStatusCode()); 
    	Assert.assertEquals(HttpStatus.BAD_REQUEST, palindromeController.palindrome(testInputTooLarge).getStatusCode());
    }
    
}
