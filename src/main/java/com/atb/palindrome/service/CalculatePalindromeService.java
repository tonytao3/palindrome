package com.atb.palindrome.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class CalculatePalindromeService {
	
	private static final Logger logger = LoggerFactory.getLogger(CalculatePalindromeService.class);
	
	/**
     * @param x int
     * @return boolean
     */
    private boolean isPalindrome(int x) {  
        long t = x, rev = 0;
        while (t > 0) {
            rev = 10 * rev + t % 10; 
            t /= 10;                   
        }
        return rev == x;
    }
  
    /**
     * @param intInput
     * @return palindrome number
     */
    public int calculatePalindrome(int intInput) {
    	
	    for (int i = 1;; i++) {
	        if (isPalindrome(intInput - i))
	            return intInput - i;
	        if (Double.valueOf(intInput) + i <= Integer.MAX_VALUE) {
		        if (isPalindrome(intInput + i))
		            return intInput + i;
	        }
	    }
    }
    
    public int closestPalindrome(int n) {
    	
    	logger.info("### - entered into closestPalindrome endpoint.");
    	char odd = '0';
    	if (n == 0) {
            return 1;
        } else if(n < 10) {
            return n-1;
        } else if(checkAll9(n)) {
            return n + 2;
        } else if (n != 1 && isPower10(n)) { //special case: 100 -> 99 palindrome is closest
            return n - 1;
        }
    	
    	logger.info("### - input passed validation.");
        String s = n + "";  // string representation of n
            
        // Take First Half of string
        String firstHalf = s.substring(0, s.length()/2);
        char[] cs = s.toCharArray();
        
        // if string is of odd length we are storing the odd digit
        if (s.length() %2 != 0) {
        	odd = cs[s.length()/2];
        }
            
        // take second half as reverse of first half
        String secondHalf = new StringBuilder(firstHalf).reverse().toString(); // reverse firstHalf;
        
        // store the palindrome candidates
        
        String pal1 = ""; // no consider the middle digital number 0 or 9
        String pal2 = ""; // move to lower side 
        String pal3 = ""; // move to higher side
        String temp;
        if(s.length()%2==1) {
            // odd length case
            pal1 = firstHalf + odd + secondHalf;
            
            if(odd == '0') {
                temp = addCarry(firstHalf, -1);
                pal2 = temp + "9" + new StringBuilder(temp).reverse().toString(); //reverse(temp);
            } else {
                pal2 = firstHalf + (Character.getNumericValue(odd) -1) + secondHalf;
            }
            
            if(odd == '9') {
                temp = addCarry(firstHalf, 1);
                pal3 = temp + '0' + new StringBuilder(temp).reverse().toString(); // reverse(temp);
            } else {
                pal3 = firstHalf + (Character.getNumericValue(odd) + 1) + secondHalf;
            }
        } else {
            // even length case
            pal1 = firstHalf + secondHalf;
            
            int smallSize = firstHalf.length(); // size of first half
            if(firstHalf.charAt(smallSize -1) == '0') {
                temp = addCarry(firstHalf,-1);
            } else {
                temp = firstHalf.substring(0,smallSize-1) + (Character.getNumericValue(firstHalf.charAt(smallSize-1)) - 1);
            }
            pal2 = temp + new StringBuilder(temp).reverse().toString(); //reverse(temp);
            
             if(firstHalf.charAt(smallSize-1) == '9') {
                temp = addCarry(firstHalf, 1);
            } else {
                temp = firstHalf.substring(0, smallSize-1) + (Character.getNumericValue(firstHalf.charAt(smallSize-1)) + 1);
            }
            pal3 = temp + new StringBuilder(temp).reverse().toString();  //reverse(temp);
        }
        if (Double.parseDouble(pal3) > Integer.MAX_VALUE) {
        	pal3 = pal2; 
        }
        // return the closest
        int diff1 =  Math.abs(n - Integer.valueOf(pal1));
        int diff2 =  Math.abs(n - Integer.valueOf(pal2));
        int diff3 =  Math.abs(n - Integer.valueOf(pal3));
        
        if(Integer.valueOf(pal1) == n) {
            return diff2 <= diff3 ?Integer.valueOf(pal2) : Integer.valueOf(pal3);
        } else if(n > Integer.valueOf(pal1))
            return diff1 <= diff3 ? Integer.valueOf(pal1) : Integer.valueOf(pal3);
        else
            return diff2 <= diff1 ? Integer.valueOf(pal2) : Integer.valueOf(pal1);
    }
    // Adding carry to num
    private static String addCarry(String strNum, int carry)
    {   
    	char[] num = strNum.toCharArray();
    	int temp;
    	int itr;
        if(carry == -1) {
            itr = num.length -1;
            while( itr >= 0 && num[itr] == '0') {
                num[itr--] = '9';
            }
            if( itr >= 0) {
                num[itr] = Integer.toString(Character.getNumericValue(num[itr])-1).charAt(0);
            }
        } else {
           for(int i = num.length -1; i >= 0; i--)
           {
               temp = Character.getNumericValue(num[i]);
               num[i] = Integer.toString((temp + carry)%10).charAt(0);
               carry = (temp + carry) / 10;
           }
        }
        
        return new String(num);
    }
    
    public static boolean isPower10(int x) {
        while (x > 9 && x % 10 == 0)
            x /= 10;
        return x == 1;
    }
    
    private static boolean checkAll9(int intInput) {
    	if (intInput>=Integer.MAX_VALUE) {
    		return false;
    	}
    	if(((intInput+1)+"").length()==(intInput+"").length()+1) {
    		return true;
    	}
    	return false;
    }

}
