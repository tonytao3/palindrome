package com.atb.palindrome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.atb.palindrome.service.CalculatePalindromeService;
import com.atb.palindrome.util.ErrorCode;
import com.atb.palindrome.util.InputValidator;

@RestController
public class PalindromeController {
	
	@Autowired
	CalculatePalindromeService  calculatePalindromeService;
	
	@Autowired
	InputValidator inputValidator;

    @GetMapping(value = "/", produces = { "text/plain" })
    public ResponseEntity<Object> index() {
        return ResponseEntity.ok().body("Hello world");
    }

    /**
     * The simplest solution is to consider every possible number smaller than the given number nn, starting by
     * decrementing 1 from the given number and go on in descending order. Similarly, we can consider every possible
     * number greater than nn starting by incrementing 1 from the given number and going in ascending order.
     * We can continue doing so in an alternate manner till we find a number which is a palindrome.
     * @param number String
     * @return ResponseEntity<Object>
     */
    @SuppressWarnings("static-access")
	@GetMapping(value = "/palindrome/{number}")
    public ResponseEntity<Object> palindrome(@PathVariable String number){
        
    	int num;
    	ErrorCode errorCode = inputValidator.validateInput(number);
    	if (!errorCode.equals(ErrorCode.NEGTIVE_INPUT) && !errorCode.equals(ErrorCode.VALID_INPUT)) {
    		return ResponseEntity.badRequest().body("Input Error: " + errorCode.getMessage());
    	} 
    	num = Integer.parseInt(number);
    	if (errorCode.equals(errorCode.NEGTIVE_INPUT)) {
    		num = Integer.parseInt(number.replace("-", ""));
    	} 
        
    	int palindromeNumber = calculatePalindromeService.calculatePalindrome(num);
          
        if (errorCode.equals(errorCode.NEGTIVE_INPUT)) {
        	return ResponseEntity.ok().body("minus sign is ignored and palindrome is:  " + palindromeNumber);
    	} 
        
        return ResponseEntity.ok().body("Palindrome is: " + palindromeNumber);
    }
    
    /**
     * This efficient solution is to find a possible palindrome number that is the closest number to the given int value, num, 
     * which is parsed from the given string number. This solution considered all possible cases that could be inputed. 
     * This approach is different from the above simple solution. It considered the special cases that the middle digit is important 
     * for the search for the closest integer to original the input. After several cases are considered and counted,
     * it then reverse the number in the left and put it to the right. This concludes the closest palindrome number.
     * The following special cases are considered and a solution ot it is developed. this cases and solutions are:
     * 1. input string contains none digital characters, a bad request 400 status code will be responded.
     * 2. When the num converted from the input string is bigger than 2147483647, a bad request 400 status code will be responded.
     * 3. when a negative number is given and if the value is in the Integer scope the minus sign will be ignored.
     * @param number String
     * @return ResponseEntity<Object>
     */
    @SuppressWarnings("static-access")
	@GetMapping(value = "/efficient-palindrome/{number}")
    public ResponseEntity<Object> efficientPalindrome(@PathVariable String number){
    	int num;
    	ErrorCode errorCode = inputValidator.validateInput(number);
    	if (!errorCode.equals(ErrorCode.NEGTIVE_INPUT) && !errorCode.equals(ErrorCode.VALID_INPUT)) {
    		return ResponseEntity.badRequest().body("Input Error: " + errorCode.getMessage());
    	} 
    	num = Integer.parseInt(number);
    	if (errorCode.equals(errorCode.NEGTIVE_INPUT)) {
    		num = Integer.parseInt(number.replace("-", ""));
    	} 
        
        int palindromeNumber = calculatePalindromeService.closestPalindrome(num);
        
        if (errorCode.equals(errorCode.NEGTIVE_INPUT)) {
        	return ResponseEntity.ok().body("minus sign is ignored and palindrome is:  " + palindromeNumber);
    	} 
        
        return ResponseEntity.ok().body("Palindrome is: " + palindromeNumber);
    }


}
