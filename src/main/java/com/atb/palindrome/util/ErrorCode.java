package com.atb.palindrome.util;

public enum ErrorCode {
	
		  INVALID_INPUT("INVALID_INPUT", "Input contains none digital character."),
		  INPUT_TOO_LONG("INPUT_TOO_LONG", "The string should not caontain converted value greate than 2147483647."),
		  VALID_INPUT("VALID_INPUT", "The string should not caontain converted value greate than 2147483647."),
		  NEGTIVE_INPUT("NEGTIVE_INPUT", "Nagtive input detected, will ignor the menus sign.");
		 
		  private final String errorCode;
		  private final String message;

		  private ErrorCode(String errorCode, String message) {
		     this.errorCode = errorCode;
		     this.message = message;
		  }

		  public String getErrorCode() { return errorCode; }
		  public String getMessage() { return message; }
		  
		  @Override
		  public String toString() {
		    return errorCode + ": " + message;
		  }
	
}
