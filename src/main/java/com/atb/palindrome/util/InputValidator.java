package com.atb.palindrome.util;

import org.springframework.stereotype.Service;

@Service
public class InputValidator {
	
	public ErrorCode validateInput(String input) {
		
		if (input == null) {
	        return ErrorCode.INVALID_INPUT;
	    } 
		
		@SuppressWarnings("unused")
		double d;
		try {                    //check for none digital characters
		     d = Double.parseDouble(input);
		} catch (NumberFormatException nfe) {
		     return ErrorCode.INVALID_INPUT;
		}
	    
		if(input.strip().replace("-", "").length()>10) {  // input is too long
			return ErrorCode.INPUT_TOO_LONG;
		} else if (input.strip().indexOf("-")==0 && Double.parseDouble(input.strip().replace("-", "")) > Integer.MAX_VALUE) { // number too big
			return ErrorCode.INPUT_TOO_LONG; 
		} else if ( Double.parseDouble(input.strip()) > Integer.MAX_VALUE) { //number too big
			return ErrorCode.INPUT_TOO_LONG; 
		} else if ( input.strip().indexOf("-")==0) { //negative number
			return ErrorCode.NEGTIVE_INPUT; 
		} 
		return ErrorCode.VALID_INPUT;
	}
}
